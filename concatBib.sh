#!/bin/bash

# I wrote this script to concat all bibtex files in a single one
# With it, I can have a clear bibliography (each them is a bib),
# but use a single file in my LaTeX file

# CC By - Pierre Maltey - July 2017
# https://creativecommons.org/licenses/by/4.0/

# Features
bold=$(tput bold)
normal=$(tput sgr0)

# Variables
OUTPATH='/home/pierre/Documents/tex/'
OUTFNAME='biblio.bib'

# Welcome message
echo "> Welcome in this script"
echo "> I will creat a single file from your differents bibliographies"
echo "> Start computation"
echo ""

################# COMPUTATIONS ############################################
if [ -e "$OUTPATH$OUTFNAME" ] # The file already exist
then
    echo "${bold}! WARNING  > $OUTPATH$OUTFNAME already exist."
    echo -n "${normal}           > Do you want erase it ? ${bold}(Y/N)${normal}  "
    read ERASEOREXIT

    case "$ERASEOREXIT" in # Do you want erase or exit ?
	[yYoO1]) echo "RESOLUTION > $OUTPATH$OUTFNAME will be erase"
		 echo "" ;;
	[nN0]) echo "           > Computation ended before generation" #If you don't erase, you exit !
	       echo "> EXIT"
	       exit ;;
	*) echo ""  # Default : you erase the file
	   echo "RESOLUTION > $OUTPATH$OUTFNAME will be erase"
	   echo "" ;;
    esac
fi


for file in ./*.bib
do
    echo 'add '"$file"
    /bin/grep -h -v -e '^$' "$file" > "$OUTPATH$OUTFNAME"
done

# Message to say "Good Bye"
echo ""
echo "> End computation"
echo "> ${bold}$OUTFNAME${normal} is generated in ${bold}$OUTPATH${normal}"


exit
