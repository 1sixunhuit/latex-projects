#!/bin/bash

# I wrote this script to clean all file created by pdfLaTeX

#ToDo : check if PDFPATH exist and if not : creat it

PDFPATH = ./pdffiles/

# Welcome message                                                                                                                                                       
echo "> Welcome in this script"
echo "> I will clean the folder"
echo ""

rm -v *~
rm -v *.aux *.log
rm -v *.toc
rm -v *.bbl *.bcf *.blg
rm -v *.xml
mv *.pdf $PDFPATH

echo "> Clean : DONE"
